---
layout: markdown_page
title: "GitLab Legal"
description: "This page lists the subprocessors that GitLab may use."
---
{::options parse_block_html="true" /}
## Sub-processors of GitLab

Gitlab may engage the following entities to carry out specific processing activities on behalf of the customer.

[Sign Up](#sign-up) to be notified of a change to the list.

### GitLab Affiliates

| Affiliate Entity |Location|Service Provided|
|---|---|---|
|GitLab BV|Netherlands|Support Services|
|GitLab IT BV|Netherlands|Support Services|
|GitLab Ltd|United Kingdom|Support Services|
|GitLab Ireland Ltd|Ireland|Support Services|
|GitLab GmbH|Germany|Support Services|
|GitLab Pty Ltd|Australia|Support Services|
|GitLab Canada Corp.|Canada|Support Services|
|GitLab GK|Japan|Support Services|
|GitLab South Korea|South Korea|Support Services

### Third Party Sub-processors

|Third Party Entity|Location|Service Provided|
|---|---|---|
|Google LLC|United States|Cloud Hosting|
|Amazon Web Services Inc. |United States|Cloud Hosting|
|Rackspace Inc.|United States|Cloud Hosting Support|
|Zendesk Inc.|Germany|Support Services|
|Elasticsearch Inc.|United States|Search Functionality|
|Cloudflare Inc.|United States and European Union|Content Delivery Network|

### Sign Up 
Complete this form to be notified of changes to our sub-processors.

<script src="//page.gitlab.com/js/forms2/js/forms2.min.js"></script>

<form id="mktoForm_2833"></form>

<script>
  var formAfterSuccessDo = function()
  {
    $('.confirmform').attr('style', 'visibility: visible');
    $('.confirmform').attr('style', 'height: initial');
    $('html, body').animate({scrollTop: parseInt($('#confirmform').offset().top-100)}, 500);
  };
  MktoForms2.setOptions(
  {
    formXDPath : "/rs/194-VVC-221/images/marketo-xdframe-relative.html"
  });
  MktoForms2.loadForm("//page.gitlab.com", "194-VVC-221", 2833, function(form) 
  {
    form.onSuccess(function()
    {
      form.getFormElem().hide();
      formAfterSuccessDo();
      return false;
    });
  });
</script>

<div class="confirmform" style="display:none;">
  <h3>Submission received</h3>
  <p>Thank you for signing up to receive updates to our subprocessor list.</p>
</div>

{::options parse_block_html="false" /}

