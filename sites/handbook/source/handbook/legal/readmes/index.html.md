---
layout: handbook-page-toc
title: "GitLab Legal Team READMEs"
description: "Get to know the Legal Team in our individual README pages"
---

## Legal Team READMEs

- [Julie Braughler (Sr. Contracts Manager)](https://about.gitlab.com/handbook/legal/readmes/juliebraughler.index.html)
- [Rashmi Chachra (Director of Legal, Corporate)](https://about.gitlab.com/handbook/legal/readmes/rashmichachra.index.html)
- [Robyn Hartough (Sr. EBA)](https://about.gitlab.com/handbook/legal/readmes/robynhartough.index.html)
- [Rob Nalen (Sr. Director of Legal Operations & Contracts)](https://about.gitlab.com/handbook/legal/readmes/robnalen.index.html)
- [Miguel Silva (Contracts Manager)](https://about.gitlab.com/handbook/legal/readmes/miguelsilva.index.html)

